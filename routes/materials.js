var express = require('express');
var users = require('../models/Users');
var passport = require('passport');
var router = express.Router();
var omppuGuard = require('../middleware/OmppuGuard');

    router.get('/', function(req, res) {
      res.status(200).send("GET not allowed.")
    });

    router.post('/list/:id', function(req, res) {
      // TODO Add implementation for material list by folder id.
    });

    router.post('/:id', function(req, res) {
      // TODO Add implementation for viewing file by id.
    });

    router.post('/add', function(req, res) {
      // TODO Add implementation for adding new files.
    });

    router.post('/delete/:id', function(req, res) {
      // TODO Add implementation for deleting files.
    });

    router.post('/rename/:id', function(req, res) {
      // TODO Add implementation for renaming folders.
    });

    router.post('/move', function(req, res) {
      // TODO Add implementation for moving files.
    });

module.exports = router;
