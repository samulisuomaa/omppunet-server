var express = require('express');
var users = require('../models/Users');
var passport = require('passport');
var router = express.Router();
var omppuGuard = require('../middleware/OmppuGuard');

    router.get('/', function(req, res) {
      res.status(200).send("GET not allowed.")
    });


    router.post('/list', function(req, res) {
      // TODO Add implementation top-tier folder list.
    });

    router.post('/:id', function(req, res) {
      // TODO Add implementation for returning folder contents.
    });

    router.post('/create', function(req, res) {
      // TODO Add implementation for creating new folders.
    });

    router.post('/delete/:id', function(req, res) {
      // TODO Add implementation for deleting folders.
    });

    router.post('/rename/:id', function(req, res) {
      // TODO Add implementation for renaming folders.
    });


module.exports = router;
