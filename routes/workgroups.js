var express = require('express');
var wg = require('../models/Workgroups');
var router = express.Router();
var passport = require('passport');
var omppuGuard = require ('../middleware/OmppuGuard');

    router.post('/list',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.getAllWorkgroups(res);
    });

    router.post('/create',
      passport.authenticate('local-login', {failureRedirect: 'authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.createNewWorkgroup(req.body.groupName, res);
    });

    router.post('/delete/:id',
      passport.authenticate('local-login', {failureRedirect: 'authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.deleteWorkgroup(req.params.id, res);
    });

    router.post('/:id/users',
      passport.authenticate('local-login', {failureRedirect: 'authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.getUsersInOneWorkgroup(req.params.id, res);
    });

    router.post('/:id/addUser',
      passport.authenticate('local-login', {failureRedirect: 'authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.addUserToWorkgroup(req.body.userID, req.params.id, res);
    });

    router.post('/:id/removeUser',
      passport.authenticate('local-login', {failureRedirect: 'authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        wg.removeUserFromWorkgroup(req.body.userID, req.params.id, res);
    });

module.exports = router;
