var express = require('express');
var bulletins = require('../models/Bulletins');
var passport = require('passport');
var router = express.Router();
var omppuGuard = require('../middleware/OmppuGuard');

  router.post('/getAll',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.getAllBulletins(res);
  });

  router.post('/getForGroup/:id',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.getBulletinsForGroup(req.params.id, res);
    });

  router.post('/getForUser',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.getBulletinsForUser(req.user.workgroup, res);
    });

  router.post('/getOneBulletin/:id',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.getOneBulletin(req.params.id, res);
    });

  router.post('/create',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.createBulletin(req.body.groupID, req.body.bulletin, res);
    });

  router.post('/delete/:id',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.deleteBulleting(req.params.id, res);
    });

  router.post('/update/:id',
    passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
    omppuGuard.requireAccessLevel(3),
    (req, res) => {
      bulletins.updateBulletin(req.params.id, req.body.updatedBulletin, res);
    });


  module.exports = router;
