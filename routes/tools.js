var express = require('express');
var tools = require('../models/Tools');
var router = express.Router();

router.get('/connection', (req, res) => {
  tools.checkOnlineStatus(res);
});

module.exports = router;
