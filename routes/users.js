var express = require('express');
var users = require('../models/Users');
var passport = require('passport');
var router = express.Router();
var omppuGuard = require('../middleware/OmppuGuard');

    router.get('/', (req, res) => {
      res.status(200).send('status: OK');
    });

    router.post('/list',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(2),
      (req, res) => {
        users.getUsernameList(res);
    });

    router.post('/free',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        users.getFreeUsers(res);
      });

    router.post('/create',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        users.registerNewUser(req.body.create_name, req.body.create_password, req.body.workgroup, res);
    });

    router.post('/:id/delete',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        users.deleteUser(req.params.id, res);
    });

    router.post('/change/:id', function(req, res) {
      // TODO Add implementation for changing user's name.
    });

    router.post('/password', function(req, res) {
      // TODO Add implementation for updating user's password.
    });

module.exports = router;
