var express = require('express');
var auth = require('../models/authenticate');
var passport = require('passport');
var router = express.Router();

    router.get('/', function(req, res) {
      res.status(200).send({status: 'error', message:'GET operation is not available on this endpoint.'});
    });

    router.get('/fail', function(req, res) {
      res.status(200).send({status: 'error', message:'Virheellinen käyttäjätunnus tai salasana!'});
    });

    router.post('/',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      (req, res) => {
        res.status(200).send({status: 'success', message:'Tunnistautuminen onnistui!', access_level: req.user.access_level});
    });

    router.put('/', function(req, res) {
        res.status(200).send({status: 'error', message:'PUT operation is not available on this endpoint.'});
    });

    router.delete('/:id', function(req, res) {
      res.status(200).send({status: 'error', message:'DELETE operation is not available on this endpoint.'});
    });

module.exports = router;
