var express = require('express');
var roles = require('../models/Roles');
var passport = require('passport');
var router = express.Router();
var omppuGuard = require('../middleware/OmppuGuard');

    router.get('/', function(req, res) {
      res.status(200).send("GET not allowed.");
    });

    router.put('/', function(req, res) {
      res.status(200).send("PUT not allowed.");
    });

    router.post('/list',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.getRolesAsList(res);
    });

    router.post('/create',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.createNewRole(req.body.roleName, req.body.accessLevel, res);
    });

    router.post('/:id/delete',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.deleteRole(req.params.id, res);
    });

    router.post('/:id/assignTo',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.assignRole(req.body.userID, req.params.id, res);
      });

    router.post('/removeFrom',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.removeRole(req.body.userID, res);
      });

    router.post('/:id',
      passport.authenticate('local-login', {failureRedirect: '/authenticate/fail', session: false}),
      omppuGuard.requireAccessLevel(3),
      (req, res) => {
        roles.getOneRole(req.params.id, res);
    });


module.exports = router;
