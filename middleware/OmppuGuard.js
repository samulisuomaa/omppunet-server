class OmppuGuard {
  requireAccessLevel(accessLevel) {
    return (req, res, next) => {
      //console.log("[OmppuGuard] Checking permission level "+ accessLevel + " for user " + req.user.username);
      if(req.user && req.user.access_level >= accessLevel) {
        //console.log("[OmppuGuard] Check successful");
        next();
      } else {
        //console.log("[OmppuGuard] Check failed.");
        res.status(401).send({status:'error', message:'Sinulla ei ole käyttöoikeutta tähän resurssiin'});
      }
    };
  };
}

module.exports = new OmppuGuard();
