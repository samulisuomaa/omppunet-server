var mysql = require('mysql');
var env = process.env.NODE_ENV;
console.log(env);
var config = require('./config/server_config')[env];

class Connection {
  constructor() {
    this.pool = null;
  }

  init() {
    console.log("Using database: " + config.database.db);
    this.pool = mysql.createPool({
      connectionLimit: config.database.connectionLimit,
      host: config.database.host,
      user: config.database.user,
      password: config.database.password,
      database: config.database.db,
      port: config.database.port
    });
  };

  acquire(callback) {
    this.pool.getConnection(function(err, connection) {
      callback(err, connection);
    });
  };
}

module.exports = new Connection();
