var env = process.env.NODE_ENV || 'development';
var config = require('./config/server_config')[env];
const express = require('express');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const auth = require('./models/Authenticate');
const bcrypt = require('bcrypt');

// MIDDLEWARE
const bodyparser = require('body-parser');
var connection = require('./connection.js');
const cors = require('cors');

const app = express();
connection.init();

// ROUTES
const users = require('./routes/users');
const authenticate = require('./routes/authenticate');
const workgroups = require('./routes/workgroups');
const roles = require('./routes/roles');
const tools = require('./routes/tools');

// PASSPORT
passport.use('local-login', new LocalStrategy(
  {session: false},
  function(username, password, cb) {
    auth.findByUsername(username, function(err, user) {
      if (err) {console.log('callback1');return cb(err);}
      if (!user) {
        return cb(null, false);
      }
      if (bcrypt.compareSync(password, user.password)) {
          return cb(null, user);
        }
      return cb(null, false);
    });
  }
));

// MIDDLEWARE CONFIGURATION
app.use(express.static('public'));
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(cors());
app.use(passport.initialize());

// ROUTE CONFIGURATION
app.use('/users', users);
app.use('/authenticate', authenticate);
app.use('/workgroups', workgroups);
app.use('/roles', roles);
app.use('/tools', tools);

// STATIC
app.use('/', express.static('public'));

var server = app.listen(config.server.port, function() {
  console.log('OmppuNet online on port  ' + server.address().port + ".");
});

module.exports = server;
