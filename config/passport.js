const auth = require('../models/Authenticate');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

passport.use('local-login', new LocalStrategy(
  {session: false},
  function(username, password, cb) {
    auth.findByUsername(username, function(err, user) {
      if (err) {console.log('callback1');return cb(err);}
      if (!user) {
        return cb(null, false, {message: 'Incorrect username'});
      }
      if (auth.verifyPassword(password, user[0].password)) {
        return cb(null, false, {message: 'Incorrect password'});
      }

      return cb(null, user);
    });
  }
));
