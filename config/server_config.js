var config = {
  development: {
    database: {
      connectionLimit: 20,
      host: 'localhost',
      user: 'root',
      password: '',
      db: 'omppunet',
      port: '3307'
    },

    server:{
      port: '8000'
    }
  },

  testing: {
    database: {
      connectionLimit: 50,
      host: 'localhost',
      user: 'root',
      password: '',
      db: 'omppunet_test',
      port: '3307'
    },

    server: {
      port: '8000'
    }
  }
};

module.exports = config;
