var connection = require('../connection.js');

/** Model for managing user roles */
class Roles {

  /**
   * Returns all roles and their information.
   * @param {function} res Response callback
   */
  getRolesAsList(res) {
    connection.acquire( (err, con) => {
      con.query('SELECT role_id, role, access_level FROM roles', (err, result) => {
        con.release();
        if(err) {
          res.status(200).send({status:'error', message:'Roolien hakeminen epäonnistui.'});
          console.log(err);
        } else {
          res.status(200).send(result);
        }
      });
    });
  }

  /**
   * Returns information for one role.
   * @param {int} roleID ID of the role
   * @param {function} res Response callback
   */
  getOneRole(roleID, res) {
    this.isRoleInDatabase(roleID, (check) => {
      if (check) {
        connection.acquire( (err, con) => {
          con.query('SELECT role_id, role, access_level FROM roles WHERE role_id=?', [roleID], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Roolin haku epäonnistui'});
              console.log(err);
            } else {
              res.status(200).send(result);
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Roolia ei ole olemassa!'});
      }
    });
  }

  /**
   * Creates a new role.
   * @param {string} roleName Name for the new role
   * @param {int} accessLevel Level of access for the role
   * @param {function} res Response callback
   */
  createNewRole(roleName, accessLevel, res) {
    this.isRoleNameFree(roleName, (check) => {
      if(check) {
        connection.acquire( (err, con) => {
          con.query("INSERT INTO roles SET role=?, access_level=?", [roleName, accessLevel], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Roolia ei voitu luoda virhetilanteen vuoksi.'});
            } else if (result.affectedRows > 0) {
              res.status(200).send({status:'success', message:'Rooli luotu.'});
            } else {
              res.status(200).send({status: 'error', message:'Virhe roolin luonnin aikana.'});
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Rooli on jo olemassa!'});
      }
    });
  }

  /**
   * Deletes the role from database.
   * @param {int} roleID ID of the role
   * @param {funcion} res Response callback
   */
  deleteRole(roleID, res) {
    this.isRoleInDatabase(roleID, (check) => {
      if(check) {
        connection.acquire( (err, con) => {
          con.query('DELETE FROM roles WHERE role_id=?', [roleID], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Roolia ei voitu poistaa virhetilanteen vuoksi.'});
            } else if (result.affectedRows > 0) {
              res.status(200).send({status:'success', message:'Rooli poistettu.'});
            } else {
              res.status(200).send({status:'error', message:'Virhe roolin poistamisen aikana'});
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Roolia ei ole olemassa!'});
      }
    });
  }

  /**
   * Assings a new role to user, it doesn't already have one
   * @param {int} userID ID of the user
   * @param {int} roleID ID of the role to be assigned
   * @param {function} res Response callback
   */
  assignRole(userID, roleID, res) {
    this.checkUserRole(userID, (check) => {
      if(check) {
        connection.acquire( (err, con) => {
          con.query('UPDATE users SET role=? WHERE user_id=?', [roleID, userID], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Roolia ei voitu lisätä käyttäjälle virhetilanteen vuoksi.'});
            } else if(result.changedRows > 0) {
              res.status(200).send({status:'success', message:'Rooli lisätty käyttäjälle.'})
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Käyttäjällä on jo rooli!'})
      }
    });
  }

  removeRole(userID, res) {
    this.checkUserRole(userID, (check) => {
      if(!check) {
        connection.acquire( (err, con) => {
          con.query("UPDATE users SET role=null WHERE user_id=?", [userID], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Roolia ei voitu poistaa virhetilanteen vuoksi.'});
            } else if(result.changedRows > 0) {
              res.status(200).send({status:'success', message:'Rooli poistettu käyttäjältä!'});
            } else {
              res.status(200).send({status:'error', message:'Jotain meni pieleen.'});
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Käyttäjällä ei ole roolia!'});
      }
    });
  }


  /**
   * Checks if the given rolename is available.
   * @param {string} roleName Name of the role to be checked
   * @param {function} cb Callback
   * @returns {boolean} true If role is available | false If not available
   */
  isRoleNameFree(roleName, cb) {
    connection.acquire( (err, con) => {
      con.query("SELECT role FROM roles WHERE role=?", [roleName], (err, result) => {
        con.release();
        if(err) {
          console.log(err);
        } else if(result.length == 0) {
          cb(true);
        } else {
          cb(false);
        }
      });
    });
  }

  /**
   * Checks if the role exists in the database.
   * @param {int} roleID ID of the role
   * @param {function} cb Callback
   */
  isRoleInDatabase(roleID, cb) {
    connection.acquire( (err, con) => {
      con.query('SELECT * FROM roles WHERE role_id=?', [roleID], (err, result) => {
        con.release();
        if(err) {
          console.log(err);
        } else if(result.length > 0) {
          cb(true);
        } else {
          cb(false);
        }
      });
    });
  }

  /**
   * Checks the the user's role.
   * @param {int} userID ID of the user
   * @param {function} cb Callback
   */
  checkUserRole(userID, cb) {
    connection.acquire( (err, con) => {
      con.query('SELECT role FROM users WHERE user_id=?', [userID], (err, result) => {
        con.release();
        if(err) {
          console.log(err);
        } else if(result.length == 0) {
          cb(false);
        } else if (result[0].role == null) {
          cb(true);
        } else {
          cb(false);
        }
      })
    });
  }

} // END OF CLASS

module.exports = new Roles();
