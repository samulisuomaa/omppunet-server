var connection = require('../connection.js');

/** Model for managing workgroups. */
class Workgroups {

  /*
   * Gets all workgroups and their ID's
   * @param {function} res Callback function
   */
  getAllWorkgroups(res) {
     connection.acquire( (err, con) => {
       con.query('SELECT group_id, group_name FROM workgroups', (err, result) => {
         con.release();
         if (err) {
           console.log(err);
         } else {
           res.status(200).send(result);
         }
       });
     });
   };

   /**
    * Gets list of users in a specific workgroup.
    * @param {group_id} group_id ID of the workgroup
    * @param {function} res Response callback
    */
   getUsersInOneWorkgroup(group_id, res) {
     connection.acquire( (err, con) => {
       con.query('SELECT user_id, username FROM users WHERE workgroup=?', [group_id], (err, result) => {
         con.release();
         if(err) {
           console.log(err);
         } else {
           res.status(200).send(result);
         }
       });
     });
   };


   /**
    * Creates a new workgroup.
    * @param {string} groupName Name of the new groupName
    * @param {function} res Callback function
    */
   createNewWorkgroup(groupName, res) {
     this.isGroupNameFree(groupName, (notTaken) => {
       if(notTaken) {
       connection.acquire( (err, con) => {
         con.query('INSERT INTO workgroups SET group_name=?', [groupName], (err, result) => {
           con.release();
           if (err) {
             console.log(err);
             res.status(200).send({status:'error', message:'Työryhmää ei voitu luoda virhetilanteen vuoksi.'});
           } else {
             res.status(200).send({status:'success', message:'Työryhmä luotu onnistuneesti.'});
           }
         });
       });
     } else {
       res.status(200).send({status:'error', message:'Työryhmän nimi on jo käytössä.'});
     }
   });
   }

   /**
    * Checks if the given groupname is already taken.
    * @param {String} groupName Name to be checked
    * @param {function} cb Callback function
    * @returns {Boolean | Boolean} false If taken | true If not taken
    */
   isGroupNameFree(groupName, cb) {
     connection.acquire( (err, con) => {
       con.query('SELECT group_name FROM workgroups WHERE group_name=?', [groupName], (err, result) => {
         con.release();
         if(err) {
           console.log(err);
         } else if (result.length == 0) {
           cb(true);
         } else {
           cb(false);
         }
       });
     });
   }

   /**
    * Deletes a workgroup.
    * @param {int} ID of the groupID
    * @param {function} Response callback function
    */
   deleteWorkgroup(groupID, res) {
     this.removeAllFromGroup(groupID, (status) => {
       if(status) {
         connection.acquire( (err, con) => {
           con.query('DELETE FROM workgroups WHERE group_id=?', [groupID], (err, result) => {
             con.release();
             if (err) {
               console.log(err);
               res.status(200).send({status:'error', message:'Jotain meni nyt pahemman kerran pieleen!'});
             } else if (result.affectedRows > 0 ) {
               res.status(200).send({status: 'success', message:'Työryhmä poistettiin onnistuneesti.'});
             } else {
               res.status(200).send({status:'error', message:'Työryhmää ei voitu poistaa.'});
             }
           });
         });
       } else {
         res.status(200).send({status: 'error', message:'Työryhmän poistaminen epäonnistui virhetilanteen vuoksi'});
       }
     });
   }

   /**
    * Removes all users from the group.
    * @param {int} groupID ID of the group
    * @param {function} cb Callback function
    * @return {Boolean} true If users were removed, otherwise false
    */
   removeAllFromGroup(groupID, cb) {
     connection.acquire( (err, con) => {
       con.query('UPDATE users SET workgroup=NULL WHERE workgroup=?', [groupID], (err, result) => {
         con.release();
         if (err) {
           console.log(err);
           cb(false);
         } else {
           cb(true);
         }
       });
     });
   }

   /**
    * Adds user to a workgroup
    * @param {int} userID ID of the user to add
    * @param {int} groupID ID of the group
    * @param {function} res Response callback function
    */
   addUserToWorkgroup(userID, groupID, res) {
     this.checkUserWorkgroup(userID, (workgroup) => {
       if(workgroup == 0) {
         connection.acquire( (err, con) => {
           con.query('UPDATE users SET workgroup=? WHERE user_id=?', [groupID, userID], (err, result) => {
             con.release();
             if(err) {
               res.status(200).send({status: 'error', message:'Käyttäjän lisääminen työryhmään epäonnstui.'});
               console.log(err);
             } else if(result.changedRows > 0) {
               res.status(200).send({status:'success', message:'Käyttäjä lisättiin onnistuneesti työryhmään.'});
             } else {
               res.status(200).send({status: 'error', message:'Käyttäjää ei voitu lisätä virhetilanteen vuoksi.'})
             }
           });
         });
       } else {
         res.status(200).send({status:'error', message:'Käyttäjä on jo jonkin työryhmän jäsen!'});
       }
     });
   }

   /**
    * Removes user from a workgroup.
    * @param {int} userID ID of the user to remove
    * @param {int} groupID ID of the group to remove from
    * @param {function} res Response callback function
    */
   removeUserFromWorkgroup(userID, groupID, res) {
     this.checkUserWorkgroup(userID, (workgroup) => {
       if(workgroup == groupID) {
       connection.acquire( (err, con) => {
         con.query('UPDATE users SET workgroup=NULL WHERE user_id=?', [userID], (err, result) => {
           con.release();
           if(err) {
             res.status(200).send({status:'error', message:'Käyttäjän poistaminen työryhmästä epäonnistui.'})
           } else if(result.changedRows > 0) {
             res.status(200).send({status:'success', message:'Käyttäjän poistaminen työryhmästä onnistui.'})
           } else {
             res.status(200).send({status:'error', message:'Käyttäjän poistaminen työryhmästä epäonnistui.'});
           }
         });
       });
     } else {
       res.status(200).send({status: 'error', message:'Käyttäjää ei ole työryhmässä!'});
     }
     });
   }

   /**
    * Checks user's current workgroup.
    * @param {int} userID ID of the user
    * @param {function} cb Callback function
    */
   checkUserWorkgroup(userID, cb) {
     connection.acquire( (err, con) => {
       con.query('SELECT workgroup FROM users WHERE user_id=?', [userID], (err, result) => {
         con.release();
         if(err) {
           console.log(err);
         } else if (result.length == 0) {
           cb(0);
         } else if(result[0].workgroup == null) {
           cb(0);
         } else {
           cb(result[0].workgroup);
         }
        });
     });
   }

 } // END OF CLASS

module.exports = new Workgroups();
