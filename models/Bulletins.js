var connection = require('../connection.js');

/** Model for managing bulletins */
class Bulletins {

  getAllBulletins(res) {
    connection.acquire( (err, con) => {
      con.query("SELECT * FROM bulletins", (err, result) => {
        con.release();
        if(err) {
          res.status(200).send({status: 'error', message:'Ilmoituksia ei voitu hakea!'});
        } else {
          res.status(200).send(result);
        }
      });
    });
  }

  getBulletinsForGroup(groupID, res) {
    connection.acquire( (err, con) => {
      con.query("SELECT * FROM bulletins WHERE target_group=?", [groupID], (err, result) =>{
        con.release();
        if(err) {
          res.status(200).send({status: 'error', message:'Ilmoituksia ei voitu hakea!'});
        } else {
          res.status(200).send(result);
        }
      });
    });
  }

  // TODO Refactor when bulletin hiding is implemented!
  getBulletinsForUser(groupID, res) {
    connection.acquire( (err, con) => {
      con.query("SELECT * FROM bulletins WHERE target_group=?", [groupID], (err, result) => {
        if(err) {
          res.status(200).send({status:'error', message:'Ilmoituksia ei voitu hakea'});
        } else {
          res.status(200).send(result);
        }
      });
    });
  }

  getOneBulletin(bulletinID, res) {
    connection.acquire( (err, con) => {
      con.query("SELECT * FROM bulletins WHERE bulletin_id=?", [bulletinID], (err, result) => {
        con.release();
        if(err) {
          res.status(200).send({status:'error', message:'Ilmoitusta ei voitu hakea!'});
        } else {
          res.status(200).send(result);
        }
      });
    });
  }

  createBulletin(groupID, bulletin, res) {
    connection.acquire( (err, con) => {
      con.query("INSERT INTO bulletins(target_group, bulletin) VALUES (?, ?)", [groupID, bulletin], (err, result) => {
        con.release();
        if(err) {
          res.status(200).send({status:'error', message:'Ilmoitusta ei voitu luoda!'});
        } else {
          res.status(200).send({status:'success', message:'Uusi ilmoitus luotu!'});
        }
      });
    });
  }

  deleteBulleting(bulletinID, res) {
    connection.acquire( (err, con) => {
      con.query("DELETE FROM bulletins WHERE bulletin_id=?", [bulletinID], (err, result) => {
        con.release();
        if(err) {
          res.status(200).send({status:'error', message:'Ilmoitusta ei voitu poistaa!'});
        } else {
          res.status(200).send({status:'success', message: 'Ilmoitus poistettu!'});
        }
      });
    });
  }

  updateBulletin(bulletinID, updatedText res) {
    connection.acquire( (err, con) => {
      con.query("UPDATE bulletins SET bulletin=? WHERE bulletin_id=?", [updatedText, bulletinID], (err, result) => {
        con.release();
        if(err){
          res.status(200).send({status:'error', message:'Ilmoitusta ei voitu päivittää!'});
        } else {
          res.status(200).send({status:'error', message: 'Ilmoitus päivitetty!'});
        }
      });
    });
  }
} // END OF CLASS

module.exports = new Bulletins();
