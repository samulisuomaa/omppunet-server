class Tools {
  checkOnlineStatus(res) {
    res.status(200).send({status:'success', message:'OmppuNet tavoitettavissa.'})
  }
} // END OF CLASS

module.exports = new Tools();
