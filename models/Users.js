var connection = require('../connection.js');
var bcrypt = require('bcrypt');
const saltRounds = 10;

/** Model for managing users */
class Users {

  /**
   * Register new user to the database.
   * @param {String} username User's username
   * @param {String} plainPassword User's password
   * @param {int} workgroup User's workgroup
   * @param {function} res Callback function
   */
  registerNewUser(username, plainPassword, workgroup, res) {
    this.isUsernameFree(username, (check) => {
      if (check) {
        bcrypt.hash(plainPassword, saltRounds, (err, hashedPassword) => {
          connection.acquire((err, con) => {
            con.query('INSERT INTO users(username, password) VALUES (?, ?)',
            [username, hashedPassword], (err, result) => {
              con.release();
              if(err) {
                console.log(err);
                res.status(200).send({status:'error', message:"Uuden käyttäjän luonti epäonnistui! Ota yhteys ylläpitoon."});
              } else if (result.affectedRows > 0) {
                res.status(200).send({status:'success', message:"Uusi käyttäjä luotu!"});
              } else {
                res.status(200).send({status:'error', message:'Virhe luonnin aikana.'});
              }
            });
          });
        });
    } else {
      res.status(200).send({status:'error', message:"Käyttäjätunnus on jo käytössä!"});
    }
    });
  }

  deleteUser(userID, res) {
    this.isUserInDatabase(userID, (check) => {
      if(check) {
        connection.acquire( (err, con) => {
          con.query("DELETE FROM users WHERE user_id=?", [userID], (err, result) => {
            con.release();
            if(err) {
              res.status(200).send({status:'error', message:'Käyttäjää ei voitu poistaa virhetilanteen vuoksi!'});
            } else if(result.affectedRows > 0) {
              res.status(200).send({status:'success', message:'Käyttäjä poistettu onnistuneesti!'});
            } else {
              res.status(200).send({status:'error', message:'Virhe käyttäjän poistamisessa.'});
            }
          });
        });
      } else {
        res.status(200).send({status:'error', message:'Käyttäjää ei ole olemassa!'});
      }
    });
  }

  getFreeUsers(res) {
    connection.acquire( (err, con) => {
      con.query("SELECT user_id, username FROM users WHERE workgroup IS NULL", (err, result) =>{
        con.release();
        if(err) {
          res.status(200).send({status:'error', message: err});
        } else if(result.length > 0) {
          res.status(200).send(result);
        } else {
          res.status(200).send({status: 'success', message: 'Yhtään vapaata työntekijää ei löytynyt!'});
        }
      });
    });
  }

  /**
   * Check if provided username is already taken.
   * @param {String} username Name to be checked
   * @return {boolean} TRUE if taken, FALSE is not
   */
  isUsernameFree(username, cb) {
    connection.acquire( (err, con) => {
      con.query('SELECT username FROM users WHERE username=?', [username], (err, result) => {
        con.release();
        if(result.length > 0) {
          cb(false);
        } else {
          cb(true);
        }
      });
    });
  }

  /**
   * Checks if user exists with given ID
   * @param {int} userID ID of the user
   * @return {boolean} true if exists
   */
  isUserInDatabase(userID, cb) {
    connection.acquire( (err, con) => {
      con.query("SELECT username FROM users WHERE user_id=?", [userID], (err, result) => {
        con.release();
        if(result.length > 0) {
          cb(true);
        } else {
          cb(false);
        }
      });
    });
  }

  /**
   * Loads a list of all user_ids and usernames from the database.
   * @param {function} res Callback function
   */
  getUsernameList(res) {
     connection.acquire( (err, con) => {
       con.query('SELECT user_id, username, group_name, roles.role FROM users LEFT OUTER JOIN workgroups ON workgroup=group_id LEFT OUTER JOIN roles ON users.role=roles.role_id',
       (err, result) => {
         con.release();
          if(err) {
            res.status(200).send("Pyyntöä ei voitu käsitellä virhetilanteen vuoksi.");
          } else {
            res.status(200).send(result);
          }
       });
     });
   }

} // END OF CLASS
module.exports = new Users();
