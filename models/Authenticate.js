var connection = require('../connection.js');
var bcrypt = require('bcrypt');

function Authenticate() {

  /**
   * Checks if the username and password match the database.
   * @param {String} username User's username
   * @param {function} cb Callback function
   * @returns {array} Array with user data.
   */
  this.findByUsername = function(username, cb) {
    connection.acquire(function(err, con) {
      con.query('SELECT username, password, workgroup, access_level FROM users INNER JOIN roles ON users.role=role_id WHERE username=?',
      [username], function(err, result) {
        con.release();
        if(err) {
          console.log(err);
          return cb(null, false);
        }
        return cb(null, result[0]);
      });
    });
  }

  this.verifyPassword = function(plainPassword, hashedPassword) {
    if(bcrypt.compare(plainPassword, hashedPassword)) {
      return true;
    }
    return false;
  }
}


module.exports = new Authenticate();
