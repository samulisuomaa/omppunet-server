CREATE DATABASE IF NOT EXISTS omppunet;

USE omppunet;

CREATE TABLE roles (
  role_id INT NOT NULL AUTO_INCREMENT,
  role VARCHAR(100) NOT NULL,
  access_level INT NOT NULL,
  PRIMARY KEY (role_id)
) ENGINE = InnoDB;

CREATE TABLE workgroups (
  group_id INT NOT NULL AUTO_INCREMENT,
  group_name VARCHAR(100) NOT NULL,
  PRIMARY KEY (group_id)
) ENGINE = InnoDB;

CREATE TABLE users (
  user_id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(100) NOT NULL,
  password VARCHAR(255) NOT NULL,
  workgroup INT,
  role INT,
  PRIMARY KEY (user_id),
  FOREIGN KEY (workgroup) REFERENCES workgroups(group_id),
  FOREIGN KEY (role) REFERENCES roles(role_id)
) ENGINE = InnoDB;

CREATE TABLE bulletins (
  bulletin_id INT NOT NULL AUTO_INCREMENT,
  target_group INT NOT NULL,
  bulletin VARCHAR(1000) NOT NULL,
  date_issued DATETIME NOT NULL DEFAULT NOW(),
  last_updated DATETIME ON UPDATE NOW(),
  PRIMARY KEY (bulletin_id),
  FOREIGN KEY (target_group) REFERENCES workgroups(group_id)
) ENGINE = InnoDB;

CREATE TABLE bulletin_visibility (
  bulletin_id INT NOT NULL,
  user_id INT NOT NULL,
  hidden INT,
  PRIMARY KEY (bulletin_id, user_id),
  FOREIGN KEY (bulletin_id) REFERENCES bulletins(bulletin_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
) ENGINE = InnoDB;

CREATE TABLE workday_comments (
  comment_id INT NOT NULL AUTO_INCREMENT,
  created DATETIME NOT NULL DEFAULT NOW(),
  last_updated DATETIME ON UPDATE NOW(),
  comment_author INT NOT NULL,
  comment VARCHAR(250) NOT NULL,
  PRIMARY KEY (comment_id),
  FOREIGN KEY (comment_author) REFERENCES users(user_id)
) ENGINE = InnoDB;

CREATE TABLE workdays (
  workday_id INT NOT NULL AUTO_INCREMENT,
  created DATETIME NOT NULL DEFAULT NOW(),
  last_updated DATETIME ON UPDATE NOW(),
  worker INT NOT NULL,
  workday DATE NOT NULL,
  start_time TIME NOT NULL,
  end_time TIME,
  total_hours DECIMAL,
  normal_hours DECIMAL,
  twenty_hours DECIMAL,
  fifty_hours DECIMAL,
  hundred_hours DECIMAL,
  workday_comment INT,
  PRIMARY KEY (workday_id),
  FOREIGN KEY (worker) REFERENCES users(user_id),
  FOREIGN KEY (workday_comment) REFERENCES workday_comments(comment_id)
) ENGINE = InnoDB;

CREATE TABLE workshifts (
  shift_id INT NOT NULL AUTO_INCREMENT,
  shift_worker INT NOT NULL,
  shift_start DATETIME NOT NULL,
  shift_end DATETIME NOT NULL,
  PRIMARY KEY (shift_id),
  FOREIGN KEY (shift_worker) REFERENCES users(user_id)
) ENGINE = InnoDB;

CREATE TABLE folders (
  folder_id INT NOT NULL,
  folder_name VARCHAR(100),
  lft INT,
  rgt INT,
  PRIMARY KEY (folder_id),
  FOREIGN KEY (lft) REFERENCES folders(folder_id),
  FOREIGN KEY (rgt) REFERENCES folders(folder_id)
) ENGINE = InnoDB;

CREATE TABLE files (
  file_id INT NOT NULL,
  file_name VARCHAR(100),
  file_path VARCHAR(100),
  folder INT NOT NULL,
  PRIMARY KEY(file_id),
  FOREIGN KEY(folder) REFERENCES folders(folder_id)
) ENGINE = InnoDB;

INSERT INTO workgroups(group_id, group_name) VALUES (1, 'admin_default');
INSERT INTO roles(role_id, role, access_level) VALUES (1, 'admin_default', 4);
INSERT INTO users(user_id, username, password, workgroup, role) VALUES (1, 'admin_default', '$2a$04$zOeE1sFXve7DUdk70amSROXSguCX.34b6fY1qP4NLwxRcNKFOyL/K',1, 1);
