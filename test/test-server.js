process.env.NODE_ENV = 'testing';
global.chai = require('chai');
global.chaiHttp = require('chai-http');
global.server = require('../server.js');
global.should = chai.should();

var connection = require('../connection');

chai.use(chaiHttp);

workgroupData = [
  [1, 'Test Group One'],
  [2, 'Test Group Two'],
  [3, 'Test Delete Group']
];

roleData = [
  [1, 'Test Role Admin', 3],
  [2, 'Test Role Normal', 1],
  [3, 'Test Role Delete', 1]
];

userData = [
  [1, 'Test User One', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 1, 1],
  [2, 'Test User Two', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 1, 2],
  [3, 'Test User Three', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, 1],
  [4, 'Test Group Delete', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 3, 2],
  [5, 'Test Group Add', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', null , 2],
  [6, 'Test Group Remove', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, 2],
  [7, 'Test Role Assign', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, null],
  [8, 'Test Role Remove', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, null],
  [9, 'Test Role Remove2', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, 2],
  [10, 'Test User Delete', '$2a$04$l1Ih8wBb8uw2l3S9.Pe2yemzRAOPanxpSQHCXBithtGDeYDTa56zu', 2, 2]

];

bulletinData = [
  [1, 1, 'Test Bulletin One'],
  [2, 2, 'Test Bulletin Two'],
  [4, 1, 'Test Delete Bulletin'],
  [5, 1, 'Test Update Bulletin']
]

global.setupTestDatabase = function(done) {
  connection.acquire( function(err, con) {
    con.query('DELETE FROM users', function(err, result) {
      if(err) {
        console.log(err);
      }
      con.query('DELETE FROM roles', function(err, result) {
        if(err) {
          console.log(err);
        }
        con.query("DELETE FROM bulletins", function(err, result) {
          if(err) {
            console.log(err);
          }
          con.query('DELETE FROM workgroups', function(err, result) {
            if(err) {
              console.log(err);
            }
            con.release();
            seedTestDatabase(done);
          });
        });
      });
    });
  });
}

seedTestDatabase = function(done) {
  connection.acquire( (err, con) => {
    con.query('INSERT INTO workgroups(group_id, group_name) VALUES ?', [workgroupData], (err, result) => {
      if (err) {
        console.log(err);
      }

      con.query('INSERT INTO roles(role_id, role, access_level) VALUES ?', [roleData], (err, result) => {
        if(err) {
          console.log(err);
        }

        con.query('INSERT INTO users(user_id, username, password, workgroup, role) VALUES ?', [userData], (err, result) => {
          if(err) {
            console.log(err);
          }
          con.query("INSERT INTO bulletins(bulletin_id, target_group, bulletin) VALUES ?", [bulletinData], (err, result) =>{
            if(err) {
              console.log(err);
            }
            con.release();
            done();
          });
        });
      });
    });
  });
}
