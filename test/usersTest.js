describe('Users', function() {
  before(function(done) {
    setupTestDatabase(done);
  });

  describe('Creating new users', () => {
    it('should create a new user in the database', (done) => {
      chai.request(server)
        .post('/users/create')
        .send({'username':'Test User One', 'password':'default', 'create_name':'Test Create User', 'create_password':'testpass'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Uusi käyttäjä luotu!');
          done();
        });
    });
    it('and check if username is already taken', (done) => {
      chai.request(server)
        .post('/users/create')
        .send({'username':'Test User One', 'password':'default', 'create_name':'Test User One', 'create_password':'testpass'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjätunnus on jo käytössä!');
          done();
        });
    });
  });

  describe('Deleting users', function() {
    it('should delete an user from the database', (done) => {
      chai.request(server)
        .post('/users/10/delete')
        .send({'username':'Test User One', 'password':'default'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Käyttäjä poistettu onnistuneesti!');
          done();
        });
    });
    it('but it should not delete if it doesnt exist', (done) => {
      chai.request(server)
        .post('/users/999/delete')
        .send({'username':'Test User One', 'password':'default'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjää ei ole olemassa!');
          done();
        });
    });
  });

  describe('Changing username', () => {
    it.skip('should change the username', () => {
      // TODO
    });
  });

  describe('Changing password', () => {
    it.skip('should check that user can only change own password', () => {
      // TODO
    });
    it.skip('should change the password', () => {
      // TODO
    })
  });

  describe('User data services', () => {
    it.skip('should return userdata as a list', () => {
      // TODO
    });
    it.skip('should return userdata for one specified user', () => {
      // TODO
    })
  });


});
