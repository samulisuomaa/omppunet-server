describe('Roles', function() {
  before(function(done) {
    setupTestDatabase(done);
  });

  describe('Getting all roles as a list', function() {
    it('should return all roles', function(done) {
      chai.request(server)
        .post('/roles/list')
        .send({'username':'Test User One', 'password':'default'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body[0].should.have.property('role_id');
          res.body[0].should.have.property('role');
          res.body[0].should.have.property('access_level');
          res.body[0].role_id.should.equal(1);
          res.body[0].role.should.equal('Test Role Admin');
          res.body[0].access_level.should.equal(3);
          res.body[1].should.have.property('role_id');
          res.body[1].should.have.property('role');
          res.body[1].should.have.property('access_level');
          res.body[1].role_id.should.equal(2);
          res.body[1].role.should.equal('Test Role Normal');
          res.body[1].access_level.should.equal(1);
          done();
        });
    });
  });

  describe('Adding new role', function() {
    it('should check that the role doesn´t exists already', (done) => {
      chai.request(server)
        .post('/roles/create')
        .send({'username':'Test User One', 'password':'default', 'roleName':'Test Role Admin', 'accessLevel':3})
        .end( (err, res) => {
          res.should.have.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Rooli on jo olemassa!');
          done();
        });
    });

    it('should add a new role to the database', (done) => {
      chai.request(server)
        .post('/roles/create')
        .send({'username':'Test User One', 'password':'default', 'roleName':'Test Create Role', 'accessLevel': 2})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Rooli luotu.');
          done();
        });
    });
  });

  describe('Deleting a role', function() {
    it('should check that the role actually exists', (done) => {
      chai.request(server)
        .post('/roles/99/delete')
        .send({'username': 'Test User One', 'password': 'default'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Roolia ei ole olemassa!');
          done();
        });
    });

    it('should delete the role', (done) => {
      chai.request(server)
        .post('/roles/3/delete')
        .send({'username': 'Test User One', 'password': 'default'})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Rooli poistettu.');
          done();
        })
    });
  });

  describe('Assigning a role', function() {
    it('should check if there is a role assigned already', (done) => {
      chai.request(server)
        .post('/roles/1/assignTo')
        .send({'username': 'Test User One', 'password':'default', 'userID':1})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjällä on jo rooli!');
          done();
        });
    });
    it('should assign the role', (done) => {
      chai.request(server)
        .post('/roles/1/assignTo')
        .send({'username': 'Test User One', 'password':'default', 'userID':7})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Rooli lisätty käyttäjälle.');
          done();
        });
    });
  });

  describe('Removing a role', () => {
    it('should check if the user has role assigned', (done) => {
      chai.request(server)
        .post('/roles/removeFrom')
        .send({'username': 'Test User One', 'password':'default', 'userID':8})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjällä ei ole roolia!');
          done();
        });
    });
    it('should remove the role from the user', (done) => {
      chai.request(server)
        .post('/roles/removeFrom')
        .send({'username': 'Test User One', 'password':'default', 'userID':9})
        .end( (err, res) => {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Rooli poistettu käyttäjältä!');
          done();
        });
    });
  });

}); // END OF ROLES TEST
