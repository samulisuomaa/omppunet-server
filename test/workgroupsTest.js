describe('Workgroups', function() {
  before(function(done) {
    setupTestDatabase(done);
  });

  describe('Getting all workgroups as a list', function() {
      it('should return data from all workgroups', function(done) {
        chai.request(server)
          .post('/workgroups/list')
          .send({'username':'Test User One', 'password':'default'})
          .end ( function(err, res) {
            res.should.have.status(200);
            res.should.be.json;
            res.body[0].should.have.property('group_id');
            res.body[0].should.have.property('group_name');
            res.body[0].group_id.should.equal(1);
            res.body[0].group_name.should.equal('Test Group One');
            res.body[1].should.have.property('group_id');
            res.body[1].should.have.property('group_name');
            res.body[1].group_id.should.equal(2);
            res.body[1].group_name.should.equal('Test Group Two');
            done();
          });
      });
    });

    describe('Getting all users in one workgroup', function() {
      it('should return users in the workgroup', function(done) {
        chai.request(server)
          .post('/workgroups/1/users')
          .send({'username': 'Test User One', 'password': 'default'})
          .end( function(err, res) {
            res.should.have.status(200);
            res.should.be.json;
            res.body[0].should.have.property('user_id');
            res.body[0].should.have.property('username');
            res.body[0].user_id.should.equal(1);
            res.body[0].username.should.equal('Test User One');
            res.body[1].should.have.property('user_id');
            res.body[1].should.have.property('username');
            res.body[1].user_id.should.equal(2);
            res.body[1].username.should.equal('Test User Two');
            done();
          });
      });
    });

  describe('Creating a new workgroup', function() {
    it('should create a new workgroup to the database', function(done) {
      chai.request(server)
        .post('/workgroups/create')
        .send({'username': 'Test User One', 'password': 'default', 'groupName': 'Create Group'})
        .end( function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.status.should.equal('success');
          done();
        });
    });

    it('should return an error if name is already taken.', function(done) {
      chai.request(server)
        .post('/workgroups/create')
        .send({'username': 'Test User One', 'password': 'default', 'groupName': 'Test Group Two'})
        .end( function(err, res) {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Työryhmän nimi on jo käytössä.');
          done();
        });
    });
  });

  describe('Deleting a workgroup', function() {
    it('should delete the workgroup', function(done) {
      chai.request(server)
        .post('/workgroups/delete/3')
        .send({'username':'Test User One', 'password':'default'})
        .end( function(err, res) {
          res.should.have.status(200);
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Työryhmä poistettiin onnistuneesti.');
          done();
        });
    });
  });

  describe('Adding users to workgroup', function() {
    it('should add the user to the workgroup', function(done) {
      chai.request(server)
        .post('/workgroups/2/addUser')
        .send({'username': 'Test User One', 'password': 'default', 'userID':5})
        .end( function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.status.should.equal('success');
          done();
        });
    });

    it('should check if the user is already in any workgroups', function(done) {
      chai.request(server)
        .post('/workgroups/2/addUser')
        .send({'username': 'Test User One', 'password': 'default', 'userID':1})
        .end( function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjä on jo jonkin työryhmän jäsen!');
          done();
        });
    });
  });

  describe('Removing user from workgroup', function() {
    it('should check if the user actually is in the workgroup', function(done) {
      chai.request(server)
        .post('/workgroups/2/removeUser')
        .send({'username': 'Test User One', 'password': 'default', 'userID': 1})
        .end( function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('error');
          res.body.message.should.equal('Käyttäjää ei ole työryhmässä!');
          done();
        });
    });

    it('should remove the user from the workgroup', function(done) {
      chai.request(server)
        .post('/workgroups/2/removeUser')
        .send({'username': 'Test User One', 'password': 'default', 'userID': 6})
        .end( function(err, res) {
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.have.property('status');
          res.body.should.have.property('message');
          res.body.status.should.equal('success');
          res.body.message.should.equal('Käyttäjän poistaminen työryhmästä onnistui.');
          done();
        });
    });
  });


}); // END OF WORKGROUP TESTS
